#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import time
import sys
import collections
import argparse
import os
import importlib
import inspect
import cv2
import IPython

__version__ = "2.2"
__author__ = "Stan Borkowski"
__copyright__ = "Copyright © by INRIA - All rights reserved - 2018"

UP_ARROW = 0
DOWN_ARROW = 1
LEFT_ARROW = 2
RIGHT_ARROW = 3
if sys.platform == "linux" or sys.platform == "linux2":
    LEFT_ARROW = 81
    RIGHT_ARROW = 83

BUFFER_LEN = 100

out = None


# --------------- module API -------------------
def process(img, **kwargs):
    """Implement your image processing and return results,
    these are passed to post_process"""
    return img


def post_process(img, processing_result, **kwargs):
    """Return an image to be displayed as output and that can be recorded."""
    return img


def mouse_cb(event, x, y, flags, param):  # mouse callback
    """You can define this method in the processing module to overwrite the
    default mouse callback for the 'main' window"""
    global out
    if event == cv2.EVENT_LBUTTONDOWN:
        value = out[y, x]
        print("{0} {1}: ".format(x, y) + str(value))


def key_cb(key_code):
    """Define this method to handle keyboard events."""
    if key_code != 255:
        print('key {} pressed'.format(key_code))


API = {'process': process,
       'post_process': post_process,
       'mouse_cb': mouse_cb,
       'key_cb': key_cb}
# ----------------------------------------------


class PySpinGrabber():
    def __init__(self, cam_no, fps, exposure, gain):
        import PySpin
        # -------------------------------------------------------------
        # system needs to be a member to prevent garbage collector from
        # destroying it
        self.system = PySpin.System.GetInstance()
        # -------------------------------------------------------------
        self.camera = self.system.GetCameras()[cam_no]
        self.camera.Init()
        print("PySpin camera {} init done".format(cam_no))
        self.camera.AcquisitionFrameRateEnable.SetValue(True)
        self.camera.AcquisitionFrameRate.SetValue(fps)
        print("Frame rate set")
        # self.camera.ExposureMode.SetValue(PySpin.ExposureAuto_Off)
        self.camera.ExposureAuto.SetValue(PySpin.ExposureAuto_Off)
        self.camera.ExposureTime.SetValue(exposure)
        print("exposure set")
        self.camera.GainAuto.SetValue(PySpin.GainAuto_Off)
        self.camera.Gain.SetValue(gain)
        print("gain set")
        # self.camera.AcquisitionMode.SetValue(PySpin.AcquisitionMode_Continous)
        self.camera.BeginAcquisition()
        print("Acquisition started")
        self.h = self.camera.Height()
        self.w = self.camera.Width()
        self.channels = 1
        self.frame_no = 0

    def read(self):
        self.frame_no += 1
        img = self.camera.GetNextImage().GetData().reshape(self.h, self.w,
                                                           self.channels)
        return True, self.frame_no, img, None

    def release(self):
        self.camera.EndAcquisition()
        self.camera.DeInit()

    def rewind(self):
        return False


class ImageListGrabber():
    def __init__(self, images):
        self.images = images
        self.index = -1

    def read(self):
        if self.index + 1 < len(self.images):
            self.index += 1
            img = cv2.imread(self.images[self.index], cv2.IMREAD_UNCHANGED)
            return True, self.index+1, img, self.images[self.index]
        else:
            img = cv2.imread(self.images[-1])
            print("last image in sequence")
            return False, self.index+1, img, self.images[-1]

    def rewind(self):
        self.index = -1
        return True

    def release(self):
        pass


class OpenCVCamGrabber():
    def __init__(self, cam_no):
        self.cap = cv2.VideoCapture(cam_no)
        if self.cap is None or not self.cap.isOpened():
            print("Unable to open camera {}".format(cam_no))
            exit(1)
        self.frame_no = 0

    def read(self):
        self.frame_no += 1
        res, frame = self.cap.read()
        return res, self.frame_no, frame, None

    def rewind(self):
        return False

    def release(self):
        self.cap.release()


class OCVseq_or_video_grabber():
    def __init__(self, video_or_sequence):
        self.cap = cv2.VideoCapture(video_or_sequence, cv2.CAP_IMAGES)
        if self.cap is None or not self.cap.isOpened():
            self.cap = cv2.VideoCapture(video_or_sequence)
            if self.cap is None or not self.cap.isOpened():
                print("Unable to open {}".format(video_or_sequence))
                exit(1)
        self.frame_no = 0

    def read(self):
        self.frame_no += 1
        # frame_count = int(self.cap.get(cv2.CAP_PROP_FRAME_COUNT))
        res, frame = self.cap.read()
        return res, self.frame_no, frame, None  # TODO could return file name if image sequence

    def rewind(self):
        self.cap.set(cv2.CAP_PROP_POS_FRAMES, 0)
        self.frame_no = 0
        return True

    def release(self):
        self.cap.release()


def is_dir(dirname):
    """Checks if a path is an actual directory or a file"""
    if not os.path.isdir(dirname):
        msg = "{0} is not a directory".format(dirname)
        raise argparse.ArgumentTypeError(msg)


def is_module(name):
    split_name = os.path.splitext(os.path.basename(name))
    if split_name[1] == ".py" or split_name[1] == "":  # load as a module
        return split_name[0]
    else:
        raise argparse.ArgumentTypeError("Must be a python module or file")


def print_fps(t):
    fps = "0"
    if t > 0:
        fps = str(round(1000/t))
    if 0 < t < 10:
        fps = "max"
    print("replay fps: " + fps)


def main():
    global out
    sys.path.append(os.getcwd())
    parser = argparse.ArgumentParser(
            description='Interactive player for OpenCV.'
                        'A processing module can be passed as argument.'
                        'Use arrow keys to set playback direction.')
    parser.add_argument('-v', '--verbose', action='store_true',
                        help='verbose mode')

    in_group = parser.add_argument_group('Input image preprocessing')
    in_group.add_argument('--in-scale', default=1.0, type=float,
                          help="scale factor for the input image")
    in_group.add_argument('-b', '--grayscale', action='store_true',
                          help="Convert image to grayscale when read")
    # in_group.add_argument('--roi', nargs=4, metavar='px', help="region of interest: x0, y0, x1, y1", type=float)
    parser.add_argument('-p', '--profile',
                        choices=['all', 'process', 'post_process'],
                        help="use cProfile to profile process and/or "
                        "post_process.")
    parser.add_argument('-a', '--autoplay', default=-1, type=int,
                        help="play input immediately and stop at n frames")
    parser.add_argument('-m', '--module', type=is_module,
                        help="python module for image processing")
    parser.add_argument('--init-opts', nargs='*', type=float,
                        help='options accepted by the processor class\' '
                        'constructor')

    out_group = parser.add_argument_group('Output options')
    out_group.add_argument('--out-scale', default=1.0, type=float,
                           help="scale factor for the output image")
    out_group.add_argument('-w', '--write-to-disk', metavar='prefix',
                           help="Write output to disk")
    parser.add_argument('-g', '--grabber',
                        choices=['OpenCV', 'PySpin'],
                        help="Use specific grabber. Default: OpenCV")
    parser.add_argument('--re-number-files', action='store_true',
                        help="Change image sequence numbering to make it "
                        "compatible with cv2.VideoCapture")
    parser.add_argument('video_or_img_list_or_cam_device', nargs='+',
                        help="Video file or a sequence of images, "
                        "or camera device number")

    args = parser.parse_args(sys.argv[1:])

    def pre_process_frame(frame):
        if args.in_scale != 1.0:
            frame = cv2.resize(frame, (0, 0), fx=args.in_scale,
                               fy=args.in_scale,
                               interpolation=cv2.INTER_NEAREST)
        if args.grayscale and frame.shape[2] == 3:
            frame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        return frame

    sequence_dir = args.video_or_img_list_or_cam_device
    ext = '.png'
    if args.re_number_files and os.path.isdir(sequence_dir):  # main argument must be a directory name
        files = os.listdir(sequence_dir)
        if len(files) > 99999:
            print("Error: To many files in the directory " + sequence_dir)
            exit(0)
        i = 0
        for i, img_file in enumerate(sorted(files)):
            ext = os.path.splitext(img_file)[1]
            new_name = '{:05}'.format(i)
            os.rename(os.path.join(sequence_dir, img_file), os.path.join(sequence_dir, new_name + ext))
        print('Done renaming ' + str(i) + ' files.')
        print('You can now call player with "' + os.path.join(sequence_dir, '%05d' + ext) + '" as argument')
        exit(0)

    # activate profiling if requested ---------------------------
    if args.profile:
        import cProfile
        import pstats
        profiler = cProfile.Profile()

    # interpret main argument -----------------------------------
    wait_time = 33  # default read speed: 30 FPS
    pause = True
    if args.autoplay >= 0:
        pause = False
    if len(sequence_dir) > 1:
        # take it as a list of image files
        cap = ImageListGrabber(sequence_dir)
    else:
        # it can be camera device number, or a video,
        # or OpenCV's magic code for image sequence or an image
        if sequence_dir[0].isdigit():  # this is a camera device number
            wait_time = 1
            pause = False  # always show live camera output
            if args.grabber == "PySpin":
                cap = PySpinGrabber(int(sequence_dir[0]), fps=15,
                                    exposure=10000, gain=10)
            else:
                cap = OpenCVCamGrabber(int(sequence_dir[0]))
        else:
            cap = OCVseq_or_video_grabber(sequence_dir[0])

    # w = cap.get(cv2.CAP_PROP_FRAME_WIDTH)
    # h = cap.get(cv2.CAP_PROP_FRAME_HEIGHT)

    # Thermal 16 bit image capture setup --------------------
    # The order of these calls matters!
    # self.cap.set(cv2.CAP_PROP_FOURCC, cv2.VideoWriter_fourcc(*"Y16 "))
    # self.cap.set(cv2.CAP_PROP_CONVERT_RGB, False)

    # init image buffer -------------------------------------
    buffer = collections.deque(maxlen=BUFFER_LEN)

    # read first image ---------------------------------------
    res, no, frame, img_file_name = cap.read()
    if res:
        frame = pre_process_frame(frame)
        buffer.appendleft((no, frame, img_file_name))

    # init main window --------------------------------------
    cv2.namedWindow('main')
    # cv2.setWindowProperty('main', cv2.WND_PROP_FULLSCREEN, cv2.WINDOW_FULLSCREEN)
    # cv2.setWindowProperty('main', cv2.WND_PROP_FULLSCREEN, cv2.WINDOW_NORMAL)

    # load processing module if provided --------------------
    if args.module:
        module = importlib.import_module(args.module)
        # find processing function or a class having a processing function or method
        for fun_name, fun_obj in inspect.getmembers(module, inspect.isfunction):
            if fun_name in API.keys():
                print("  Using function " + fun_name)
                API[fun_name] = fun_obj

        if API['process'] == process:
            # if a class is present use it as processor
            for cl_name, cl_obj in inspect.getmembers(module, inspect.isclass):
                if cl_obj.__module__ == module.__spec__.name:
                    print("  Using class " + cl_name)
                    if args.init_opts:
                        proc = cl_obj(*args.init_opts)
                    else:
                        proc = cl_obj()
                    # get all useful methods of proc
                    all_callables = inspect.getmembers(proc, inspect.ismethod) + inspect.getmembers(proc, inspect.isfunction)
                    for m_name, m_obj in all_callables:
                        if m_name in API.keys():
                            print("  Using function {}.{}".format(cl_name, m_name))
                            API[m_name] = m_obj

    cv2.setMouseCallback('main', API['mouse_cb'])

    write = False
    direction = 1
    buffer_index = 0
    while True:
        proceed = False
        # image processing --------------------------
        frame_no = buffer[buffer_index][0]
        img = buffer[buffer_index][1]
        img_file_name = buffer[buffer_index][2]
        start = time.time()
        if args.profile == 'all' or args.profile == 'process':
            profiler.enable()
            results = API['process'](img, frame_no=frame_no)
            profiler.disable()
        else:
            results = API['process'](img, frame_no=frame_no)
        end = time.time()

        # convert to color if needed ----------------
        if len(img.shape) == 2:
            out = cv2.cvtColor(img, cv2.COLOR_GRAY2BGR)
        else:
            out = img.copy()
        # scale image if needed ---------------------
        if args.out_scale != 1.0:
            out = cv2.resize(out, (0, 0), fx=args.out_scale, fy=args.out_scale,
                             interpolation=cv2.INTER_NEAREST)
        if args.profile == 'all' or args.profile == 'post_process':
            profiler.enable()
            out = API['post_process'](out, results,
                                      frame_no=frame_no,
                                      scale=args.out_scale,
                                      file_name=img_file_name)
            profiler.disable()
        else:
            out = API['post_process'](out, results,
                                      frame_no=frame_no,
                                      scale=args.out_scale,
                                      file_name=img_file_name)

        # write to disk -----------------------------
        if write:
            if args.write_to_disk:
                if not os.path.isdir(args.write_to_disk):
                    os.mkdir(args.write_to_disk)
                cv2.imwrite(os.path.join(args.write_to_disk, '{:06}.png'.format(frame_no)), out)
            else:
                cv2.imwrite('{:06}.png'.format(frame_no), out)

        cv2.imshow('main', out)
        elapsed_t = (end - start) * 1000
        if img_file_name:
            title = f"frame {frame_no}: {img_file_name}: {elapsed_t:.1f} ms"
        else:
            title = f"frame {frame_no}: {elapsed_t:.1f} ms"
        cv2.setWindowTitle("main", title)

        if pause:
            k = cv2.waitKey(0) & 0xFF
        else:
            k = cv2.waitKey(wait_time) & 0xFF

        if k == ord('q'):
            break
        elif k == 255:  # waitKey timeout
            proceed = True
        elif k == ord('r'):  # record output
            write = not write
        elif k == ord('i'):  # enter Ipython console
            if args.module:
                print("\nThe module variable is: proc or module\n")
            IPython.embed()
            continue
        elif k == ord(' '):  # pause replay when space is pressed
            pause = not pause
        elif k == ord('0'):  # rewind to start
            if cap.rewind():
                buffer.clear()
                buffer_index = 0
                direction = 1
                res, no, frame, img_file_name = cap.read()
                if res:
                    frame = pre_process_frame(frame)
                    buffer.appendleft((no, frame, img_file_name))
            continue
        elif k == ord('1'):  # set slow playback
            wait_time = 500
            print_fps(wait_time)
            continue
        elif k == ord('2'):  # set normal playback
            wait_time = 33  # 30 fps
            print_fps(wait_time)
            continue
        elif k == ord('3'):  # set max speed playback
            wait_time = 1
            print_fps(wait_time)
            continue
        elif k == UP_ARROW:  # speed up
            wait_time = int(wait_time/2)
            print_fps(wait_time)
            continue
        elif k == DOWN_ARROW:  # slow down
            wait_time = wait_time*2
            print_fps(wait_time)
            continue
        elif k == LEFT_ARROW:  # backward playback
            direction = -1
            proceed = True
        elif k == RIGHT_ARROW:  # forward playback
            direction = 1
            proceed = True
        else:
            API['key_cb'](k)

        if not proceed:
            continue

        if direction == 1:
            if buffer_index == 0:  # moving forward in the sequence
                res, no, frame, img_file_name = cap.read()
                if res:
                    frame = pre_process_frame(frame)
                    buffer.appendleft((no, frame, img_file_name))
                else:
                    pause = True
            if buffer_index > 0:  # moving forward within the buffer
                buffer_index -= 1
        else:
            if buffer_index + 1 < BUFFER_LEN and frame_no-1 > 0:  # moving backward in the buffer
                buffer_index += 1
        if buffer_index == BUFFER_LEN - 1:
            print("end of buffer")
            pause = True
        if frame_no == 0 and direction == -1:
            print("first frame")
            pause = True
        if args.autoplay == no:
            args.autoplay = -1
            pause = True

    cap.release()
    cv2.destroyAllWindows()
    if args.profile == 'all' or args.profile == 'process' or args.profile == 'post_process':
        stats = pstats.Stats(profiler).sort_stats('cumtime')
        stats.print_stats()
    return


if __name__ == "__main__":
    main()
