import cv2
import numpy as np


class Crop:
    def __init__(self, x0, y0, x1, y1):
        self.x0 = int(x0)
        self.y0 = int(y0)
        self.x1 = int(x1)
        self.y1 = int(y1)

    def post_process(self, img, res):
        return img[self.y0:self.y1, self.x0:self.x1,:]
