#  Copyright (c) 2020. Inria

from setuptools import setup, find_packages

setup(
    name="ocv_player",
    version="1.0",
    packages=find_packages(),
    entry_points={
        'console_scripts': [
            'ocv_player=player:main'
        ]
    }
)
