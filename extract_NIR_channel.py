import cv2
import numpy as np

"""Use this module with the interactive 'r' (record) key"""


def process(img, **kwargs):
    if img.shape[2] != 4:
        print("This module works only with 4-channel images")
        exit(1)

    nir = img[:, :, 3]
    nir3ch = cv2.cvtColor(nir, cv2.COLOR_GRAY2BGR)
    return nir3ch


def post_process(img, res, **kwargs):
    sp = kwargs['file_name'].rsplit("/", 1)
    if len(sp) == 2:
        d, f = sp
        fname = d + "/3ch_nir_" + f
    else:  # local file
        fname = "3ch_nir_" + sp[0]
    print(f"writing {fname}")
    cv2.imwrite(fname, res)
    return res
